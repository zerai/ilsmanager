<?php

use App\Assembly;
use App\Votation;
use App\Vote;
use Illuminate\Database\Seeder;


use App\Movement;
use App\Role;
use App\RoleUser;
use App\User;
use App\Account;
use App\AccountRule;
use App\AccountRow;
use App\Bank;
use App\Section;
use App\Fee;
use App\Refund;
use App\Receipt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Role::all()->isEmpty()) {
            $this->getOrCreateRole('admin');
            $this->getOrCreateRole('member');
            $this->getOrCreateRole('referent');
        }

        if (Section::all()->isEmpty()) {
            $this->getOrCreateSectionByCityAndProv('Roma',   'RM');
            $this->getOrCreateSectionByCityAndProv('Milano', 'MI');
        }

        if (Account::all()->isEmpty()) {
            $accounts = [
                'Soci' => [
                    'Quote Associative'
                ],
                'Attività Istituzionale' => [
                    'Sponsorizzazioni',
                    'Donazioni'
                ],
                'Beni e Servizi' => [
                    'Stampa',
                    'Materiale Promozionale',
                    'Posta e Spedizioni',
                    'Banche',
                    'Commercialista',
                    'Consulenze',
                    'Servizi Web',
                    'Hardware',
                ],
                'Imposte e Tasse' => []
            ];

            foreach ($accounts as $parent => $sub_accounts) {
                $p = new Account();
                $p->name = $parent;
                $p->parent_id = null;
                $p->save();

                foreach ($sub_accounts as $sa) {
                    $s = new Account();
                    $s->name = $sa;
                    $s->parent_id = $p->id;
                    $s->save();
                }
            }

            Account::where('name', 'Quote Associative')->update(['fees' => true]);
            Account::where('name', 'Donazioni')->update(['donations' => true]);
            Account::where('name', 'Banche')->update(['bank_costs' => true]);
        }

        $time = strtotime("-1 year", time());
        $previous_year = date("Y", $time);

        // Create Banks and their AccountRule.
        if (Bank::all()->isEmpty()) {
            $bank = new Bank();
            $bank->name = 'Unicredit';
            $bank->type = 'unicredit';
            $bank->identifier = 'IT74G0200812609000100129899';
            $bank->save();

            $r = new AccountRule();
            $r->bank_id = $bank->id;
            $r->rule = 'IMPOSTA BOLLO CONTO CORRENTE';
            $r->account_id = 10;
            $r->notes = 'Imposta bollo';
            $r->save();

            $bank = new Bank();
            $bank->name = 'PayPal';
            $bank->type = 'paypal';
            $bank->identifier = 'direttore@linux.it';
            $bank->save();

            $r = new AccountRule();
            $r->bank_id = $bank->id;
            $r->rule = '^Hetzner Online';
            $r->account_id = 13;
            $r->notes = 'VPS';
            $r->save();
        }

        // Create Users and some example Movements and AccountRow.
        if (User::all()->isEmpty()) {
            $user = new User();
            $user->username = 'admin';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->name = 'Amministratore';
            $user->surname = 'ILS';
            $user->email = 'admin@example.com';
            $user->notes = '';
            $user->birth_prov = 'RM';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Roma', 'RM')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = "1970-01-01";
            $user->save();
            $this->grantUserRole($user, 'admin');
            $user_admin = $user;

            $movement = new Movement();
            $movement->amount = 25;
            $movement->bank_id = $bank->id;
            $movement->date = date('Y-m-d');
            $movement->identifier = '';
            $movement->notes = "Importazione quota";
            $movement->save();

            $ar = new AccountRow();
            $ar->movement_id = $movement->id;
            $ar->account_id = Account::where('name', 'Quote Associative')->first()->id;
            $ar->amount_in = $user->feeAmount();
            $ar->user_id = $user->id;
            $ar->notes = AccountRow::createFeeNote($user, $previous_year);
            $ar->save();

            $fee = new Fee();
            $fee->account_row_id = $ar->id;
            $fee->user_id = $user->id;
            $fee->year = $previous_year;
            $fee->save();

            $receipt = new Receipt();
            $receipt->movement_id = $movement->id;
            $receipt->causal = AccountRow::createFeeNote($user, $previous_year);
            $receipt->date = $previous_year . date('-m-d');
            $receipt->header = 'Ricevuta';
            $receipt->save();

            $movement = new Movement();
            $movement->amount = 25;
            $movement->bank_id = $bank->id;
            $movement->date = date('Y-m-d');
            $movement->identifier = '';
            $movement->notes = "Importazione quota";
            $movement->save();

            $ar = new AccountRow();
            $ar->movement_id = $movement->id;
            $ar->account_id = Account::where('name', 'Quote Associative')->first()->id;
            $ar->amount_in = $user->feeAmount();
            $ar->user_id = $user->id;
            $ar->notes = AccountRow::createFeeNote($user, date('Y'));
            $ar->save();

            $receipt = new Receipt();
            $receipt->movement_id = $movement->id;
            $receipt->causal = AccountRow::createFeeNote($user, date('Y'));
            $receipt->date = date('Y-m-d');
            $receipt->header = 'Ricevuta';
            $receipt->save();

            $fee = new Fee();
            $fee->account_row_id = $ar->id;
            $fee->user_id = $user->id;
            $fee->year = date('Y');
            $fee->save();

            $user = new User();
            $user->username = 'member';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->name = 'Membro';
            $user->surname = 'ILS';
            $user->email = 'member@example.com';
            $user->notes = '';
            $user->birth_prov = 'RM';
            $user->address_prov = 'RM';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Roma', 'RM')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = "1990-01-01";
            $user->save();
            $this->grantUserRole($user, 'member');
            $user_member = $user;

            $fee = new Fee();
            $fee->account_row_id = $ar->id;
            $fee->user_id = $user->id;
            $fee->year = date('Y');
            $fee->save();

            $user = new User();
            $user->username = 'referent';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->name = 'Referente';
            $user->surname = 'ILS';
            $user->email = 'referent@example.com';
            $user->notes = '';
            $user->birth_prov = 'RM';
            $user->address_prov = 'MI';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Milano', 'MI')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = "1980-01-01";
            $user->save();
            $this->grantUserRole($user, 'referent');
            $user_referent_milano = $user;

            $fee = new Fee();
            $fee->account_row_id = $ar->id;
            $fee->user_id = $user->id;
            $fee->year = date('Y');
            $fee->save();

            $refund = new Refund();
            $refund->amount = 20;
            $refund->user_id = $user_referent_milano->id;
            $refund->section_id = $this->getOrCreateSectionByCityAndProv('Milano', 'MI')->id;
            $refund->date = date('Y-m-d');
            $refund->refunded = 1;
            $refund->notes = 'Chiesto rimborso';
            $refund->admin_notes = 'Rimborso fatto Milano';
            $refund->save();

            $user = new User();
            $user->username = 'pending';
            $user->password = Hash::make($user->username);
            $user->status = 'pending';
            $user->type = 'regular';
            $user->name = 'In attesa';
            $user->surname = 'ILS';
            $user->email = 'pending@example.com';
            $user->notes = '';
            $user->birth_prov = 'MI';
            $user->address_prov = 'MI';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Milano', 'MI')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = "1995-01-01";
            $user->save();
            $this->grantUserRole($user, 'member');
            $user_pending = $user;

            $fee = new Fee();
            $fee->account_row_id = $ar->id;
            $fee->user_id = $user->id;
            $fee->year = date('Y');
            $fee->save();

            $user = new User();
            $user->username = 'suspended';
            $user->password = Hash::make($user->username);
            $user->status = 'suspended';
            $user->type = 'regular';
            $user->name = 'Sospeso';
            $user->surname = 'ILS';
            $user->email = 'suspended@example.com';
            $user->notes = '';
            $user->birth_prov = 'MI';
            $user->address_prov = 'RM';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Roma', 'RM')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            $this->grantUserRole($user, 'member');
            $user_suspended = $user;

            $fee = new Fee();
            $fee->account_row_id = $ar->id;
            $fee->user_id = $user->id;
            $fee->year = 2020;
            $fee->save();

            $user = new User();
            $user->username = 'expelled';
            $user->password = Hash::make($user->username);
            $user->status = 'expelled';
            $user->type = 'regular';
            $user->name = 'Espulso';
            $user->surname = 'ILS';
            $user->email = 'expelled@example.com';
            $user->notes = '';
            $user->birth_prov = 'FI';
            $user->address_prov = 'RM';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Roma', 'RM')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            $this->grantUserRole($user, 'member');
            $user_expelled = $user;

            $user = new User();
            $user->username = 'dropped';
            $user->password = Hash::make($user->username);
            $user->status = 'dropped';
            $user->type = 'regular';
            $user->name = 'Eliminato';
            $user->surname = 'ILS';
            $user->email = 'dropped@example.com';
            $user->notes = '';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Milano', 'MI')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->save();
            $this->grantUserRole($user, 'member');
            $user_dropped = $user;

            $user = new User();
            $user->username = 'association';
            $user->password = Hash::make($user->username);
            $user->status = 'active';
            $user->type = 'regular';
            $user->name = 'Associazione';
            $user->surname = 'ILS';
            $user->email = 'association@example.com';
            $user->notes = '';
            $user->birth_prov = 'FI';
            $user->address_prov = 'MI';
            $user->section_id = $this->getOrCreateSectionByCityAndProv('Milano', 'MI')->id;
            $user->request_at = $user->approved_at = date('Y-m-d');
            $user->birth_date = "2000-01-01";
            $user->save();
            $this->grantUserRole($user, 'member');
            $user_association = $user;

            $fee = new Fee();
            $fee->account_row_id = $ar->id;
            $fee->user_id = $user->id;
            $fee->year = date('Y');
            $fee->save();
        }

        if (Assembly::all()->isEmpty()) {
            $assembly = new Assembly();
            $assembly->date = '2099-12-31';
            $assembly->status = 'pending';
            $assembly->announce = 'Assemblea futura';
            $assembly->save();

            $assembly = new Assembly();
            $date = new DateTime(now());
            $interval = new DateInterval('P1M');
            $date->add($interval);
            $assembly->date = $date->format('Y-m-d');
            $assembly->status = 'open';
            $assembly->announce = 'Assemblea attiva';
            $assembly->save();

            $assembly = new Assembly();
            $assembly->date = "0001-01-01";
            $assembly->status = 'closed';
            $assembly->announce = 'Assemblea chiusa';
            $assembly->save();
        }

        if (Votation::all()->isEmpty()) {
            // TODO: Add some Votations with multiple answers (not just multiple options).
            $votation = new Votation();
            $votation->assembly_id = 2;
            $votation->question = 'Votazione in attesa';
            $votation->status = 'pending';
            $votation->options = ["a","b","c"];
            $votation->save();

            $votation = new Votation();
            $votation->assembly_id = 2;
            $votation->question = 'Votazione in corso';
            $votation->status = 'running';
            $votation->options = ["d","e","f"];
            $votation->save();

            $votation = new Votation();
            $votation->assembly_id = 2;
            $votation->question = 'Votazione chiusa';
            $votation->status = 'closed';
            $votation->options = ["g","h","i"];
            $votation->save();
        }

        if (Vote::all()->isEmpty()) {
            $max_votations = 5;
            $max_users_in_votation = 5;

            // Find votation Candidates.
            $possible_votations = Votation::inRandomOrder()->take($max_votations)->get()->all();
            while($possible_votation = array_pop($possible_votations)) {

                // Find user candidates.
                $possible_users = User::inRandomOrder()->take($max_users_in_votation)->get()->all();
                while($possible_user = array_pop($possible_users)) {

                    // Pick a random answer.
                    // TODO: Check if the Votation supports multiple answers.
                    $random_votation_options = $possible_votation->options;
                    shuffle($random_votation_options);
                    $vote_answer = array_slice($random_votation_options, 0, 1);

                    // Create vote.
                    $vote = new Vote();
                    $vote->votation_id = $possible_votation->id;
                    $vote->user_id = $possible_user->id;
                    $vote->answer = $vote_answer;
                    $vote->save();
                }
            }
        }
    }

    /**
     * Get or create a Role by name.
     * @param string $name
     * @return Role
     */
    private function getOrCreateRole($name)
    {
        // Get already-existing role.
        $role = Role::where('name', $name)->first();
        if($role)
        {
            return $role;
        }

        // Create Role on-the-fly.
        $role = new Role();
        $role->name = $name;
        return $role->save();
    }

    private function grantUserRole(User $user, string $role_name)
    {
        $role_id = $this->getOrCreateRole($role_name)->id;
        $user->roles()->attach($role_id);
    }

    private function getOrCreateSectionByCityAndProv(string $city, string $prov)
    {

       // Get already-existing Section.
       $section = Section::where('city', $city)->where('prov', $prov)->first();
       if($section)
       {
           return $section;
       }

       // Create Section on-the-fly.
       $section = new Section();
       $section->city = $city;
       $section->prov = $prov;
       return $section->save();
    }
}
