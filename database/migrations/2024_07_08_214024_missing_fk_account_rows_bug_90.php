<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    // These were defined as "int(10) unsigned NOT NULL DEFAULT 0".
    // Now we want these as  "int(10) unsigned          DEFAULT NULL".
    private $ils_columns_with_zero = ['account_id', 'user_id', 'section_id'];

    /**
     * Run the migrations.
     */
    public function up(): void
    {
        // Drop "DEFAULT 0".
        // Drop "NOT NULL" constraint.
        Schema::table('account_rows', function (Blueprint $table) {
//          $table->integer('account_id')->unsigned()->nullable(false)              ->change(); // This was mandatory, keep mandatory.
            $table->integer('user_id',  )->unsigned()->nullable(true)->default(null)->change();
            $table->integer('section_id')->unsigned()->nullable(true)->default(null)->change();
        });

        // Promote NULL to "0" again.
        foreach($this->ils_columns_with_zero as $column_with_zero) {
            DB::table('account_rows')
                ->where($column_with_zero, 0)
                ->update([
                    $column_with_zero => null,
                ]);
        }

        // Add foreign keys. This implicitly also create indexes.
        Schema::table('account_rows', function (Blueprint $table) {
            $table->foreign('account_id')->references('id')->on('accounts')->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('user_id')   ->references('id')->on('users')   ->onDelete('restrict')->onUpdate('restrict');
            $table->foreign('section_id')->references('id')->on('sections')->onDelete('restrict')->onUpdate('restrict');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        // Drop foreign keys (note: this keeps related indexes).
        Schema::table('account_rows', function (Blueprint $table) {
            $table->dropForeign(['account_id']);
            $table->dropForeign(['user_id']);
            $table->dropForeign(['section_id']);
        });
        // Drop also related indexes.
        // Unfortunately, it seems that we need to drop by exact name.... D: D: D:
        Schema::table('account_rows', function (Blueprint $table) {
            $table->dropIndex('account_rows_account_id_foreign');
            $table->dropIndex('account_rows_user_id_foreign');
            $table->dropIndex('account_rows_section_id_foreign');
        });

        // Promote "0" to NULL, otherwise foreign key makes no sense.
        foreach($this->ils_columns_with_zero as $column_with_zero) {
            DB::table('account_rows')
                ->where($column_with_zero, null)
                ->update([
                    $column_with_zero => 0,
                ]);
        }

        // Add "DEFAULT 0".
        // Add "NOT NULL" constraint.
        Schema::table('account_rows', function (Blueprint $table) {
//          $table->integer('account_id')->unsigned()->nullable(false)            ->change(); // This was mandatory, keep mandatory.
            $table->integer('user_id',  )->unsigned()->nullable(false)->default(0)->change();
            $table->integer('section_id')->unsigned()->nullable(false)->default(0)->change();
        });
    }
};
