# Documentazione amministrazione con ILS Manager

Documentazione sulla gestione dei soci di un'associazione, tramite il gestionale "ILS Manager".

Questa documentazione è indirizzata al "direttore" (/ "segretario") dell'associazione o altri incarichi anche amministrativi.

Nota: tutti gli esempi hanno come URL quello dell'ILS Manager di ILS.

## Nuovi soci

I nuovi soci si registrano sulla pagina apposita. Esempio per ILS:

https://ilsmanager.linux.it/ng/register

Questi testi si possono personalizzare da questo file:

```
./resources/views/auth/register.blade.php
```

A questo punto i soci solitamente effettuano il pagamento della quota, chi con PayPal e chi con bonifico.

Quando una nuova persona effettua l'iscrizione, il direttore riceve una mail di notifica. È modificabile da questo file:

```
./resources/views/email/new_user.blade.php
```

### Elenco nuovi soci

Il direttore dell'associazione, ogni volta che un nuovo socio si registra, riceve una email.

Quando ciò avviene, nella pagina dei soci, in alto, sono elencati quelli con quota non ancora pagata (secondo il Manager):

https://ilsmanager.linux.it/ng/user

### Verifica nuovi soci

Per verificare un nuovo socio al momento è necessario:

* importare i nuovi movimenti PayPal (veloce procedura semi-automatica)
* importare i nuovi movimenti bancari (lenta procedura manuale, bisogna anche avere accesso all'home banking)

Importante: Finchè la quota di un socio non viene importata e revisionata, il socio non riceve alcuna e-mail di benvenuto.

## Importazione movimenti PayPal

Se i soci possono pagare la quota tramite PayPal puoi visitare la pagina dei Movimenti e selezionare il pulsante "Importa da API PayPal".

Fatto. In seguito, visita la scheda "XXX Movimenti da Revisionare".

#### Importazione movimenti bancari

Se i soci possono pagare la quota tramite bonifico bancario, accedi al tuo home banking.

Cerca la sezione dei movimenti ed esportali, possibilmente in formato "testo separato con delimitatori".

Nota: in Unicredit si esporta dal menu CONTI CORRENTI > Saldo e movimenti, e lo fa in formato "Testo con delimitatori" `.wri`. In realtà è in CSV (con separatore punto e virgola) e va bene.

Importante: in Unicredit, devi avere il browser in lingua inglese, altrimenti non avrai importi generati in formato corretto `123.34`.

Filtri consigliati solo se si lavora da soli (da soli è facile evitare di perdere movimentazioni o averle duplicate):

* Data di inizio: rispetto all'ultima importazione, impostare la data *successiva*
* Data di fine: impostare la data di ieri

Filtri da adottare se ci sono altre persone che collaborano sui movimenti (e rendere impossibile la perdita di movimenti ma causando eventuali duplicati da dover verificare):

* Data di inizio: rispetto all'ultima importazione, impostare la *stessa* data
* Data di fine: impostare la data di ieri

In seguito, importa quel file dalla pagina dei Movimenti, selezionando la banca e "Importa dal file movimenti":

https://ilsmanager.linux.it/ng/movement

Fatto. In seguito, visita la scheda "XXX Movimenti da Revisionare".

Verifica eventuali movimenti duplicati, in particolare nel primo giorno di importazione.

## Movimenti da Revisionare

Una volta importati (da PayPal o importazioni varie) i movimenti devono essere revisionati manualmente.

### Movimenti da Revisionare: Quote associative

Il sistema dovrebbe riconoscere il nome del socio dalla causale della transazione.

Idealmente una quota associativa importata in ILS Manager dovrà avere questa causale:

```
Saldo quota ANNO Cognome Nome
```

Selezionare correttamente il socio corrispondente dal menù a tendina.

Ripulire (impostando su "Nessuna") la colonna sulla selezione della sede locale.

Una volta che la prima quota viene salvata, il sistema invia automaticamente una e-mail di benvenuto e di istruzioni al socio coinvolto.

Questa e-mail può essere personalizzata cambiando questo file:

```
./app/Mail/FirstFee.php
```

### Movimenti da Revisionare: Donazioni all'associazione

Per donazioni dirette alla vostra associazione:

Ripulire (impostando su "Nessuno") la colonna sulla selezione del socio.

Ripulire (impostando su "Nessuna") la colonna sulla selezione della sede locale.

### Movimenti da Revisionare: Donazioni o spese sezioni locali.

Selezionare correttamente la sede locale.

Ripulire (impostando su "Nessuno") la colonna sulla selezione del socio.

### Movimenti da Revisionare: movimenti da dividere

In caso di movimenti da dividere (per esempio un bonifico da 30€ in cui 25€ sono quota associativa e 5€ donazione), allora creare tutti i relativi sotto-movimenti in modo coerente.

(Per esempio: assicurati che il primo movimento abbia un'ammontare coerente con la quota, e poi crea un'altra riga con il pulsante "aggiungi" per impostare come donazione i soldi rimanenti, ovvero 5€).

## Nuova sezione locale

Quando è richiesta l'apertura di una nuova sezione locale:

* ILS Manager > Sezioni Locali > sezione locale > Registra Nuova

A questo punto, associare tutti i soci.

In seguito, aggiornare anche sui seguenti repository:

* https://github.com/Gelma/LugMap (aggiornare directory `db` - nota: basta ci sia una voce con ILS nel nome)
* https://gitlab.com/ItalianLinuxSociety/ils.org (aggiornare directory `sezionilocali`)

## Licenza

Ogni contributo a questa documentazione è in CC0, pubblico dominio.

https://creativecommons.org/publicdomain/zero/1.0/
