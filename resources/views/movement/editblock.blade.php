<ul class="list-group">
    <li class="list-group-item">
        <div class="row">
            <div class="col-md-3">
                {{ $movement->date }}<br />
                <small>{{ $movement->bank->type }}</small>
            </div>
            <div class="col-md-3">
                {{ $movement->amount }}
            </div>
            <div class="col-md-6">
                {{ $movement->notes }}
            </div>
        </div>
    </li>

    <!-- Account Rows Sum warning: start -->
    @if( !$movement->account_rows->isEmpty() && !$movement->isAmountMatchingAccountRows() )
    <li class="list-group-item">
        <div class="alert alert-danger account-rows-sum-danger" role="alert">
          <p><b>Errore Sommatoria</b></p>
          <p>Correggere gli importi delle sottostanti righe contabili, affinché il loro totale corrisponda a quello del movimento bancario.<br />
             Scarto rilevato:
             <b class="account-rows-amount-diff-error">{{ sprintf('%.02f', $movement->getAccountRowsAmountDiffError() ) }}</b> EUR
          </p>
        </div>
    </li>
    @endif
    <!-- Account Rows Sum warning: end -->

    @if(!$movement->account_rows->isEmpty())
        @foreach($movement->account_rows as $ar)
            @include('accountrow.editblock', ['ar' => $ar])
        @endforeach
    @elseif(($guessed = $movement->guessRows())->isEmpty() == false)
        @foreach($guessed as $ar)
            @include('accountrow.editblock', ['ar' => $ar])
        @endforeach
    @else
        @include('accountrow.editblock', ['ar' => null])
    @endif

    <li class="list-group-item">
        <div class="form-check float-right">
            <input class="form-check-input" type="checkbox" name="remove[]" value="{{ $movement->id }}" id="remove{{ $movement->id }}">
            <label class="form-check-label" for="remove{{ $movement->id }}">
                Elimina
            </label>
        </div>
        <button class="btn btn-default btn-sm add-row"><span class="oi oi-plus"></span></button>
        <ul class="hidden">
            @include('accountrow.editblock', ['ar' => null])
        </ul>
    </li>
</ul>
