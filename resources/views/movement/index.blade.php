@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-9">

            <!-- start form upload movements file -->
            <form class="form" method="post" action="{{ route('movement.store') }}" enctype="multipart/form-data">
                @csrf
                <input type="hidden" name="store_from" value="file" />

                @foreach(App\Bank::orderBy('name')->get() as $bank)
                    <div class="form-check form-check-inline">
                        <input class="form-check-input" id="bank_{{ $bank->id }}" type="radio" name="bank_id" value="{{ $bank->id }}" required>
                        <label class="form-check-label" for="bank_{{ $bank->id }}">{{ $bank->name }} (ultimo aggiornamento: <a href="{{ route('bank.list', $bank->id) }}" target="_blank">{{ $bank->last_update }}</a>)</label>
                    </div>
                @endforeach

                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Importa da file movimenti</button>
                    <input type="file" name="file" />
                </div>
            </form>
            <!-- end form upload movements file -->

            <hr />

            <!-- start form PayPal API -->
            @if( PayPalAPIMovementsReader::hasValidConfig() )
                <form class="form" method="post" action="{{ route('movement.store') }}">
                    @csrf
                    <input type="hidden" name="store_from" value="paypal_api" />
                    <button type="submit" class="btn btn-primary">Importa da API PayPal</button>
                </form>
            @endif
            <!-- end form PayPal API -->

        </div>
        <div class="col-md-3">
            @if(App\Movement::whereDoesntHave('account_rows')->count() != 0)
                <a href="{{ route('movement.review') }}" class="btn btn-primary float-right">{{ App\Movement::whereDoesntHave('account_rows')->count() }} Movimenti da Revisionare</a>
            @endif
        </div>
    </div>

    <hr>

    <nav class="nav mt-3">
        @foreach(App\Movement::selectRaw('YEAR(date) as year')->distinct()->orderBy('year', 'asc')->pluck('year') as $nav_year)
            <a class="nav-link{{ $year == $nav_year ? ' active font-weight-bold' : '' }}" href="{{ route('movement.index', ['year' => $nav_year]) }}">{{ $nav_year }}</a>
        @endforeach
    </nav>

    <canvas id="chart"></canvas>

    <div class="row mt-3">
        <div class="col-md-12">
            <ul class="list-group">
                <?php

                function collectByAccount($account, $year) {
                    $row = (object)[
                        'name' => $account->name,
                        'in' => 0,
                        'out' => 0,
                        'sum' => 0,
                        'children' => []
                    ];

                    $row->in = App\AccountRow::where('account_id', $account->id)->whereHas('movement', function($query) use ($year) {
                        $query->where(DB::raw('YEAR(date)'), $year);
                    })->sum('amount_in');

                    $row->out = App\AccountRow::where('account_id', $account->id)->whereHas('movement', function($query) use ($year) {
                        $query->where(DB::raw('YEAR(date)'), $year);
                    })->sum('amount_out');

                    $row->sum = $row->in - $row->out;

                    foreach($account->children as $child) {
                        $c = collectByAccount($child, $year);
                        if ($c->sum != 0) {
                            $row->children[] = $c;
                            $row->in += $c->in;
                            $row->out += $c->out;
                            $row->sum += $c->sum;
                        }
                    }

                    return $row;
                }

                function displayByAccount($row, $deep) {
                    echo sprintf('<li class="list-group-item">
                        <div class="row">
                            <div class="col-md-%s offset-md-%s">%s</div>

                            <div class="col-md-2">
                                <span class="float-right badge badge-success">%s €</span>
                            </div>
                            <div class="col-md-2">
                                <span class="float-right badge badge-danger">%s €</span>
                            </div>
                            <div class="col-md-2">
                                <span class="float-right">%s €</span>
                            </div>
                        </div>
                    </li>', 6 - $deep, $deep, htmlentities( $row->name ), sprintf('%.02f', $row->in), sprintf('%.02f', $row->out), sprintf('%.02f', $row->sum));

                    foreach($row->children as $child) {
                        displayByAccount($child, $deep + 1);
                    }
                }

                $total_in = 0;
                $total_out = 0;
                $total_sum = 0;

                foreach(App\Account::whereNull('parent_id')->orderBy('name', 'asc')->get() as $account) {
                    $row = collectByAccount($account, $year);
                    displayByAccount($row, 0);
                    $total_in += $row->in;
                    $total_out += $row->out;
                    $total_sum += $row->sum;
                }

                ?>

                <li class="list-group-item">
                    <div class="row">
                        <div class="col-md-6"><strong>Totale</strong></div>

                        <div class="col-md-2">
                            <span class="float-right badge badge-success"><strong>{{ sprintf('%.02f', $total_in) }} €</strong></span>
                        </div>
                        <div class="col-md-2">
                            <span class="float-right badge badge-danger"><strong>{{ sprintf('%.02f', $total_out) }} €</strong></span>
                        </div>
                        <div class="col-md-2">
                            <span class="float-right"><strong>{{ sprintf('%.02f', $total_sum) }} €</strong></span>
                        </div>
                    </div>
                </li>
            </ul>
        </div>
    </div>

    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Data</th>
                        <th>Valore</th>
                        <th>Account</th>
                        <th>Note</th>
                        <th>Visualizza</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objects as $row)
                        <tr class="{{ $row->amount_in > 0 ? 'table-success' : 'table-danger' }}">
                            <td>{{ $row->movement->date }}</td>
                            <td>{{ $row->amount }}</td>
                            <td>{{ $row->account ? $row->account->printable_name : 'Utente non definito' }}</td>
                            <td>{{ $row->notes }}</td>
                            <td><a href="{{ route('movement.edit', $row->movement->id) }}"><span class="oi oi-pencil"></span></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

<script>
    const labels = ['gen', 'feb', 'mar', 'apr', 'mag', 'giu', 'lug', 'ago', 'set', 'ott', 'nov', 'dic'];
    const data = {
        labels: labels,
        datasets: [
            {
                label: 'Entrate',
                data: @json( array_values($incomes) ),
                borderColor: '#00aa00',
                backgroundColor: 'rgba(50, 170, 50, 0.8)',
            },
            {
                label: 'Uscite',
                data: @json( array_values($costs) ),
                borderColor: '#aa0000',
                backgroundColor: 'rgba(190, 30, 30, 0.8)',
            },
            {
                label: 'Saldo',
                data: @json( array_values($revenue) ),
                borderColor: '#4490e7',
                type: 'line',
                borderDash: [20, 20],
                backgroundColor: 'rgba(14, 45, 81, 0.4)',
            }
        ]
    };

    const config = {
        type: 'bar',
        data: data,
        options: {
            responsive: true
        },
    };

    addEventListener("DOMContentLoaded", (event) => {
        const ctx = document.getElementById('chart');

        new Chart(ctx, config);
    });

</script>
@endsection
