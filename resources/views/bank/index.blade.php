@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            Per tutti i movimenti di tutte le banche, clicca <a href="{{ route('bank.list_all') }}">qui</a>.
        </div>
        <div class="col-md-12">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#createBank">Crea Nuova</button>
            <div class="modal fade" id="createBank" tabindex="-1" role="dialog">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Crea Nuova</h5>
                            <button type="button" class="close" data-dismiss="modal">
                                <span>&times;</span>
                            </button>
                        </div>
                        <form method="POST" action="{{ route('bank.store') }}">
                            @csrf
                            <div class="modal-body">
                                @include('bank.form', ['object' => null])
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annulla</button>
                                <button type="submit" class="btn btn-primary">Salva</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row mt-3">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>Identificativo</th>
                        <th>Modifica</th>
                        <th>Movimenti</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($objects as $bank)
                        <tr>
                            <td>{{ $bank->name }}</td>
                            <td>{{ $bank->identifier }}</td>
                            <td><a href="{{ route('bank.edit', $bank->id) }}"><span class="oi oi-pencil"></span></a></td>
                            <td><a href="{{ route('bank.list', $bank->id) }}?year={{ date('Y') }}"><span class="oi oi-clipboard"></span></a></td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
