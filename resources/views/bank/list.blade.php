@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 mt-3">

            @if ($limit > 0)
                <h5 id="bank-last-movements">Ultimi {{ $limit }} Movimenti Bancari</h5>
            @else
                <h5 id="bank-last-movements">Movimenti Bancari</h5>
            @endif

            @if ($bank)
                <p>Dal conto {{ $bank->name }}</p>
            @endif

            <p>Ogni Movimento bancario è dettagliato una o più Righe Contabili.</p>
            <form method="GET" action="" class="auto-filter-form">
                <div class="row mt-3">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="year">Anno:</label>
                            <select id="year" name="year" class="form-control">
                                <option value="-" @selected($year === null)>Tutti</option>

                                @foreach(array_reverse(range(date('Y')-10, date('Y'))) as $available_year)
                                    <option value="{{ $available_year }}" @selected($year == $available_year)>{{ $available_year }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    @if ($limit > 0)
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="limit">Limite:</label>
                            <input type="number" id="limit" name="limit" class="form-control" value="{{ $limit }}">
                        </div>
                    </div>
                    @endif
                </div>
            </form>

            <table class="table">
                <thead>
                    <tr>
                        <th><!-- Actions --></th>
                        <th colspan="3">Movimenti Bancari</th>
                        <th colspan="6" class="border-left">Movimenti in Dettaglio (Righe Contabili)</th>
                    </tr>
                    <tr>
                        <th><!-- Actions --></th>
                        <th>Data<br />Movimento</th>
                        <th>Causale<br />Originale</th>
                        <th>Importo<br />Bancario</th>
                        <th class="border-left">Dettaglio<br />Importo</th>
                        <th>Account<br />Contabile</th>
                        <th>Causale<br />Riga Contabile</th>
                        <th>Utente<br />&nbsp;</th>
                        <th>Sezione<br />Locale</th>
                        <th>Anno<br />Quota</th>
                        @if (empty($id))
                        <th>Banca</th>
                        @endif
                    </tr>
                </thead>
                <tbody>
                        @foreach($movements_dto as $movement_dto)

                            <tr>
                                @if($movement_dto['account_row_first'])
                                    <td><a href="{{ route('movement.edit', $movement_dto['movement']->id) }}" title="Modifica Movimento {{ $movement_dto['movement']->id }}"><span class="oi oi-pencil"></span></a></td>
                                    <td>{{ $movement_dto['movement']->date }}</td>
                                    <td>{{ $movement_dto['movement']->notes }}</td>
                                    <td>{{ $movement_dto['movement']->amount }}</td>
                                @else
                                    <td><!-- Edit Movement already printed --></td>
                                    <td><abbr title="{{ $movement_dto['movement']->date }}">&hellip;</abbr></td>
                                    <td><abbr title="{{ $movement_dto['movement']->notes }}">&hellip;</abbr></td>
                                    <td><abbr title="{{ $movement_dto['movement']->amount }}">&hellip;</abbr></td>
                                @endif

                                <td class="border-left {{ $movement_dto['movement']->isAmountMatchingAccountRows() ?: 'bg-danger' }}">
                                    {{ $movement_dto['account_row'] ? $movement_dto['account_row']->printableAmount : '' }}
                                </td>

                                <td>{{ $movement_dto['account_row'] ? $movement_dto['account_row']->account->name : '' }}</td>
                                <td>{{ $movement_dto['account_row'] ? $movement_dto['account_row']->notes : '' }}</td>

                                <td>
                                @if( $movement_dto['account_row'] && $movement_dto['account_row']->user )
                                <a href="{{ route('user.edit', $movement_dto['account_row']->user->id) }}">{{ $movement_dto['account_row']->user->printableName }}</a>
                                @endif
                                </td>

                                <td>
                                @if( $movement_dto['account_row'] && $movement_dto['account_row']->section )
                                <a href="{{ route('section.edit', $movement_dto['account_row']->section->id) }}">{{ $movement_dto['account_row']->section->city }}</a>
                                @endif
                                </td>

                                <td>{{ $movement_dto['fee'] ? $movement_dto['fee']->year : '' }}</td>
                                @if (empty($id))
                                <td><a href="{{ route('bank.edit', $movement_dto['movement']->bank_id) }}">{{ $banks->where('id', $movement_dto['movement']->bank_id)->first()->name }}</a></td>
                                @endif
                            </tr>

                        @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
