@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <form method="POST" action="{{ route('user.update', $object->id) }}">
                @method('PUT')
                @csrf

                @include('user.form', ['object' => $object])

                <hr>

                <div class="form-group row">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Salva</button>
                    </div>
                </div>
            </form>

            @if($currentuser->hasRole('admin') && $object->status == 'pending')
                <form method="POST" action="{{ route('user.destroy', $object->id) }}">
                    @method('DELETE')
                    @csrf

                    <div class="form-group row">
                        <div class="col-sm-10">
                            <button type="submit" class="btn btn-danger">Elimina</button>
                        </div>
                    </div>
                </form>
            @endif
        </div>
        <div class="col-md-3 offset-md-1">
            <h5>Quote Versate</h5>

            @if($object->fees()->count() == 0)
                <div class="alert alert-danger">
                    Nessuna quota versata sinora.
                </div>
            @else
                <ul class="list-group">
                    @foreach($object->fees()->orderBy('year', 'desc')->get() as $fee)
                        <li class="list-group-item">
                            {{ $fee->year }}

                            @if($currentuser->hasRole('admin') && isset($fee->account_row))
                                <a href="{{ route('movement.edit', $fee->account_row->movement_id) }}" title="Modifica Movimento {{ $fee->account_row->movement_id }}"><span class="oi oi-pencil"></span></a>
                            @endif

                            @if($fee->receipt())
                                <span class="float-right"><a href="{{ $fee->receipt()->link }}">Ricevuta {{ $fee->receipt()->number }}</a></span>
                            @endif
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
</div>
@endsection
