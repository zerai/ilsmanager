<?php

if (!isset($name))
    $name = 'user[]';

if (!isset($select))
    $select = 0;

if (!isset($status)) {
    $status = array_map(function($n) {
        return $n->identifier;
    }, App\User::statuses());
}
else if (!is_array($status)) {
    $status = [$status];
}

$classes = 'form-control';
if(isset($extra_classes)) {
    $classes .= " $extra_classes";
}
?>

<select class="{{ $classes }}" name="{{ $name }}" autocomplete="off">
    <option value="0" {{ $select == 0 ? 'selected' : '0' }}>Nessuno</option>
    @foreach(App\User::whereIn('status', $status)->orderBy('surname', 'asc')->get() as $user)
        <option value="{{ $user->id }}" {{ $select == $user->id ? 'selected' : '' }}>{{ $user->printable_name }} ({{ $user->username }})</option>
    @endforeach
</select>
