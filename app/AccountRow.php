<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * The AccountRow is a logical money transfer.
 * The AccountRow always involve an Account (example: Donations, Fees, etc.)
 * The AccountRow may involve a single User and/or a single Section.
 * The AccountRow may also rappresent internal money transfer.
 * When we receive a Movement, one or multiple AccountRow(s) are created.
 */
class AccountRow extends Model
{
    public function movement()
    {
        return $this->belongsTo('App\Movement');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function section()
    {
        return $this->belongsTo('App\Section');
    }

    public function fee()
    {
//      // Under the hood is technically possible to have multiple Fees, by mistake,
//         but it's still a mistake! not a feature! So we want just one here.
//      return $this->hasMany('App\Fee')->with('user');
        return $this->hasOne('App\Fee')->with('user');
    }

    public function getAmountAttribute()
    {
        return $this->amount_in - $this->amount_out;
    }

    /*
        Questa funzione genera una stringa che sia poi leggibile da
        getFeeYear(). Permette di identificare i movimenti che sono stati a
        priori identificati come pagamenti di quote di iscrizione, per poi
        appunto poterle registrare in quanto quote.
    */
    public static function createFeeNote($user, $year)
    {
        return sprintf('Saldo quota %s %s', $year, $user->printable_name);
    }

    public static function createDonationNote($name)
    {
        return sprintf('Donazione per %s', $name);
    }

    public function getFeeYear()
    {
        return preg_replace('/^Saldo quota ([0-9]{4}) .*$/', '\1', $this->notes);
    }

    public function getPrintableAmountAttribute()
    {
        if (!empty($this->amount_in) && $this->amount_in != 0) {
            // this is a string
            return $this->amount_in;
        }
        else {
            // this is float
            return ($this->amount_out * -1);
        }
    }
}
