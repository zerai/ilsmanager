<?php

namespace App\Observers;

use Log;

use App\Movement;

class MovementObserver
{
    public function deleted(Movement $movement)
    {
        foreach($movement->account_rows as $row) {
            $row->delete();
        }
    }
}
