<?php

namespace App\Policies;

use App\User;
use App\Vote;
use Illuminate\Auth\Access\HandlesAuthorization;

class VotePolicy
{
    use HandlesAuthorization;

    public function view(User $user, Vote $model)
    {
        return true;
    }

    public function create(User $user)
    {
        return true;
    }

    public function update(User $user, Vote $model)
    {
        return false;
    }

    public function delete(User $user, Vote $model)
    {
        return false;
    }
}
