<?php
/**
    ILS Manager
    Copyright (C) 2021-2024 Italian Linux Society, contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as
    published by the Free Software Foundation, either version 3 of the
    License, or (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

use PayPal\PayPalAPI\TransactionSearchReq;
use PayPal\PayPalAPI\TransactionSearchRequestType;
use PayPal\PayPalAPI\GetTransactionDetailsReq;
use PayPal\PayPalAPI\GetTransactionDetailsRequestType;
use PayPal\Service\PayPalAPIInterfaceServiceService;

/**
 * PayPal API movements reader
 *
 * Originally from:
 *   https://github.com/ItalianLinuxSociety/Donazioni/blob/master/read_paypal.php
 *
 * @author Valerio Bozzolan, Roberto Guido
 */
class PayPalAPIMovementsReader
{
    /**
     * Process the results
     *
     * @param array $args Array of arguments. All of them:
     *   startDate:     DateTime When to start your search (default: endDate - 6 months)
     *   endDate:       DateTime When to stop your search (default: today)
     *   itemCallback:  callable function
     *     It will be called for each Movement with these arguments:
     *       itemCallback( $id, $date, $amount, $item, $details )
     *     Where:
     *       $id      string   The Movement identified. Example: 'ASD12312313JHJAJKDJAS'
     *       $date    DateTime The Movement date
     *       $amount  float  Example: 25.00
     *       $fee     float  Example: 1.20
     *       $causal  string Example: 'BOB TI AMO PRENDI I MIEI SOLDI PER GHANOO PLUS LEENOCS'
     *       $item    string Example: 'ASD786876786809889908ASD'
     *       $details object
     *
     *     Note that if you return FALSE the loop is interrupted, otherwise
     *     you will get all the movements.
     */
    public static function process($args = [])
    {
        $date_start = isset($args['startDate']) ? $args['startDate'] : null;
        $date_end   = isset($args['endDate']) ? $args['endDate']   : null;

        if (!$date_end) {
            $date_end = new DateTime();
        }

        if (!$date_start) {
            $date_start = clone $date_end;
            $date_start->sub(new DateInterval('P4M'));
        }

        if (!isset($args['itemCallback'])) {
            abort(500, "missing itemCallback argument");
        }

        // PayPal wants this format for dates
        $raw_date_start = $date_start->format('Y-m-d\TH:i:s\Z');
        $raw_date_end   = $date_end  ->format('Y-m-d\TH:i:s\Z');

        $transactionSearchRequest = new TransactionSearchRequestType();
        $transactionSearchRequest->StartDate = $raw_date_start;
        $transactionSearchRequest->EndDate   = $raw_date_end;

        $tranSearchReq = new TransactionSearchReq();
        $tranSearchReq->TransactionSearchRequest = $transactionSearchRequest;

        $paypalService = new PayPalAPIInterfaceServiceService(self::configCredentialsForService());

        // Some Transactions are "nonsense temporary bank stuff".
        // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/89
        $nonsense_transaction_types = [
            'Temporary Hold',
            'Authorization',
        ];

        try {
            $transactionSearchResponse = $paypalService->TransactionSearch($tranSearchReq);
            $errors = $transactionSearchResponse->Errors ?? null;
            if (!$transactionSearchResponse || $transactionSearchResponse->Ack !== 'Success' || isset($errors)) {
                abort(500, sprintf("unexpected response from PayPal. ACK: %s. Errors: %s", $transactionSearchResponse->Ack, json_encode($errors)));
            }

            $payment_transactions = $transactionSearchResponse->PaymentTransactions ?? [];
            foreach ($payment_transactions as $trans) {

                // Some Transactions are "nonsense temporary bank stuff".
                // https://gitlab.com/ItalianLinuxSociety/ilsmanager/-/issues/89
                if (isset($trans->Type) && in_array($trans->Type, $nonsense_transaction_types, true)) {
                    continue;
                }

                $req = new GetTransactionDetailsRequestType();
                $req->TransactionID = $trans->TransactionID;
                $tranreq = new GetTransactionDetailsReq();
                $tranreq->GetTransactionDetailsRequest = $req;
                $details = $paypalService->GetTransactionDetails($tranreq);

                $item = $details->PaymentTransactionDetails->PaymentItemInfo->PaymentItem;

                if (is_array($item) && $item[0]->Name !== '') {

                    // Sometime PayPal returns every single field with just 'null'. Skip this nonsense data.
                    if(!isset($details->PaymentTransactionDetails->PaymentInfo->GrossAmount->value)) {
                       continue;
                    }

                    $date = \DateTime::createFromFormat('Y-m-d\TH:i:s\Z', $details->Timestamp);

                    if ($details->PaymentTransactionDetails->ReceiverInfo->Receiver == 'direttore@linux.it') {
                        $payer = $details->PaymentTransactionDetails->PayerInfo;
                        $amount = (float) $details->PaymentTransactionDetails->PaymentInfo->GrossAmount->value;
                        $causal = sprintf('%s %s - %s - %s', $payer->PayerName->FirstName, $payer->PayerName->LastName, $payer->Payer, $item[0]->Name);
                    } else {
                        $receiver = $details->PaymentTransactionDetails->ReceiverInfo;
                        $amount = ((float) $details->PaymentTransactionDetails->PaymentInfo->GrossAmount->value) * -1;
                        $causal = sprintf('%s - %s - %s', $receiver->Receiver, $receiver->Receiver, $item[0]->Name);
                    }

                    $fee = 0;
                    if (isset($details->PaymentTransactionDetails->PaymentInfo->FeeAmount)) {
                        $fee = (float) $details->PaymentTransactionDetails->PaymentInfo->FeeAmount->value;
                    }

                    if (isset($args['itemCallback'])) {
                        $result = call_user_func($args['itemCallback' ], $trans->TransactionID, $date, $amount, $fee, $causal, $item, $details);

                        if ($result === false) {
                            break;
                        }
                    }
                }
            }
        } catch (\Exception $ex) {
            // in the future eventually do something else instead of exploding
            // but maybe exploding is just fine
            throw $ex;
        }
    }

    /**
     * Get an array of credentials for PayPalAPIInterfaceServiceService
     *
     * @return array
     */
    private static function configCredentialsForService()
    {
        // no credentials no party
        $config = self::validatedConfig();

        // initialize config from given credentials
        $paypal_config = [
               'mode'            => 'live',
               'acct1.UserName'  => $config['username'],
               'acct1.Password'  => $config['password'],
               'acct1.Signature' => $config['signature'],
        ];

        return $paypal_config;
    }

    /**
     * Check if you have a valid configuration
     *
     * Do not care about whatever issue about it
     *
     * @return boolean
     */
    public static function hasValidConfig()
    {
        $ok = false;

        try {

            // this can throw an exception
            self::validatedConfig();

            // yuppie!
            $ok = true;
        } catch (\Exception $e) {

            // oh now :(
        }

        return $ok;
    }

    /**
     * Get the whole validated PayPal service configuration
     *
     * Note that this throws exceptions if something is wrong.
     *
     * @return array
     */
    public static function validatedConfig()
    {

        // no PayPal info no party
        $info = config('services.paypal');
        if (!$info) {
            throw new \Exception("missing PayPal configuration - check your ENV file");
        }

        // mandatory fields with the related ENV file (check config/services.php)
        $mandatory_fields_and_env = [

            // useful to login in PayPal APIs
            'username'  => 'PAYPAL_USERNAME',
            'password'  => 'PAYPAL_PASSWORD',
            'signature' => 'PAYPAL_SIGNATURE',

            // useful to keep a relation to the Bank ID in the database
            'bankid'    => 'PAYPAL_BANKID',
        ];

        // check each mandatory field
        foreach ($mandatory_fields_and_env as $field => $env) {

            // no field no party
            if (!isset($info[ $field ])) {

                // be very verbose because I can be totally drunk when configuring this stuff
                throw new \Exception(sprintf(
                    "missing PayPal configuration field '%s'. Check %s in your .env file and read the fantastic manual",
                    $field,
                    $env
                ));
            }
        }

        return $info;
    }
}
