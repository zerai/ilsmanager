<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;

use App\Config;
use App\Account;

class ConfigController extends Controller
{
    public function index()
    {
        $this->checkAuth();
        return view('config.index');
    }

    public function update(Request $request, $id)
    {
        $this->checkAuth();

        /*
            Tutte le configurazioni
        */

        foreach(Config::allConfig() as $identifier => $metadata) {
            if ($request->has($identifier)) {
                $config = Config::where('name', $identifier)->first();

                if ($metadata->type == 'boolean')
                    $config->value = '1';
                else
                    $config->value = $request->input($identifier);

                $config->save();
            }
            else {
                if ($metadata->type == 'boolean') {
                    $config = Config::where('name', $identifier)->first();
                    $config->value = '0';
                    $config->save();
                }
            }
        }

        if (Config::getConfig('custom_email_aliases') == '1') {
            /*
                E-mail extra
            */

            $ex_extra_mail = json_decode(Config::getConfig('custom_email_extras'));

            $extra_mail = $request->input('custom_email_extras_email', []);
            $extra_mail_password = $request->input('custom_email_extras_password', []);
            $em = [];

            foreach($extra_mail as $index => $extra) {
                if (empty($extra_mail[$index])) {
                    continue;
                }

                $email = $extra_mail[$index];

                if (empty($extra_mail_password[$index])) {
                    $password = null;

                    foreach($ex_extra_mail as $ex_em) {
                        if ($ex_em[0] == $email) {
                            $password = $ex_em[1];
                            break;
                        }
                    }
                }
                else {
                    $password = mailPasswordHash($extra_mail_password[$index]);
                }

                if (empty($password)) {
                    continue;
                }

                $em[] = [$email, $password];
            }

            $config = Config::where('name', 'custom_email_extras')->first();
            $config->value = json_encode($em);
            $config->save();
        }

        /*
            Accounting
        */

        $account_ids = $request->input('account_id');
        $account_names = $request->input('account_name');
        $account_archived = $request->input('account_archived', []);
        $account_parents = $request->input('account_parent');
        $removed = $request->input('account_removed', []);

        foreach($account_ids as $index => $id) {
            if ($id == 0) {
                $account = new Account();
            }
            else {
                $account = Account::find($id);

                if (in_array($id, $removed)) {
                    $account->delete();
                    continue;
                }
            }

            $name = trim($account_names[$index]);
            if (empty($name)) {
                continue;
            }

            $account->name = $name;
            $account->archived = in_array($id, $account_archived);
            $account->parent_id = $account_parents[$index] == 0 ? null : $account_parents[$index];
            $account->save();
        }

        return redirect()->route('config.index');
    }
}
