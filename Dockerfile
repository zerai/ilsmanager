FROM debian:bookworm

RUN apt-get update \
 && DEBIAN_FRONTEND=noninteractive \
    apt-get install --yes \
      composer \
      apache2 \
      libapache2-mod-php \
      php-cli \
      php-mysql \
      php-pdo \
      php-gd \
      php-dom \
      php-curl \
 && apt-get clean

COPY ./entrypoint.sh /entrypoint.sh

WORKDIR /var/www/html

EXPOSE 8000/tcp

ENTRYPOINT [ "/entrypoint.sh" ]
